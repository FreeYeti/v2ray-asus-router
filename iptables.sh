#!/bin/bash

# iptables
# --append  -A chain		Append to chain
# --new     -N chain		Create a new user-defined chain
# --destination -d address[/mask][...]
# --jump	-j target       target for rule
# --table	-t table	table to manipulate (default: `filter')
# --delete  -D chain		Delete matching rule from chain
iptables -t nat -N V2RAY
iptables -t nat -A V2RAY -p tcp -j RETURN -m mark --mark 0xff

# https://en.wikipedia.org/wiki/Reserved_IP_addresses
iptables -t nat -A V2RAY -d 0.0.0.0/8 -j RETURN
iptables -t nat -A V2RAY -d 10.0.0.0/8 -j RETURN
iptables -t nat -A V2RAY -d 100.64.0.0/10 -j RETURN
iptables -t nat -A V2RAY -d 127.0.0.0/8 -j RETURN
iptables -t nat -A V2RAY -d 169.254.0.0/16 -j RETURN
iptables -t nat -A V2RAY -d 172.16.0.0/12 -j RETURN
iptables -t nat -A V2RAY -d 192.0.0.0/24 -j RETURN
iptables -t nat -A V2RAY -d 192.0.2.0/24 -j RETURN
iptables -t nat -A V2RAY -d 192.88.99.0/24 -j RETURN
iptables -t nat -A V2RAY -d 192.168.0.0/16 -j RETURN
iptables -t nat -A V2RAY -d 198.18.0.0/15 -j RETURN
iptables -t nat -A V2RAY -d 198.51.100.0/24 -j RETURN
iptables -t nat -A V2RAY -d 203.0.113.0/24 -j RETURN
iptables -t nat -A V2RAY -d 240.0.0.0/4 -j RETURN
iptables -t nat -A V2RAY -d 255.255.255.255/32 -j RETURN

# https://www.iana.org/assignments/multicast-addresses/multicast-addresses.xhtml
iptables -t nat -A V2RAY -d 224.0.0.0/4 -j RETURN

# Redirect ip packages to V2ray
iptables -t nat -A V2RAY -p tcp -j REDIRECT --to-ports 12345
iptables -t nat -A PREROUTING -p tcp -j V2RAY
iptables -t nat -A OUTPUT -p tcp -j V2RAY

# Stop v2ray
# iptables -D V2RAY
