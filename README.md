# V2RAY for ASUS Router Routers

## About

run v2ray on ASUS routers with out plugin

## Requirements

* To use this project, please download and install `merlin`(`https://www.asuswrt-merlin.net/download`) first.
* Activate SSH on your router (Login to router admin page > Advanced Settings > System > Service)
* Recommend use `SWAP` on your router
* Prepare a V2RAY server before start and test it's worked.

## Use

1. Download latest version of v2ray-core for arm64 and unzip to `/tmp/mnt/UDISK/v2ray-asus-router/v2ray` (Use ./download in Unix/Linux)

```bash
cd v2ray-asus-router
./download
```

2. Copy `./v2ray-asus-router/config-template.json` to `./v2ray-asus-router/config.json` and edit it

3. Copy `./v2ray-asus-router` to `/tmp/mnt/UDISK/v2ray-asus-router` on your `Router`

example:

```bash
rm -rf ./.git .gitignore ./v2ray-linux-arm.zip ./download ./config-template.json
scp -r ../v2ray-asus-router router:/tmp/mnt/UDISK/v2ray-asus-router
```

4. initialize

```bash
cd /tmp/mnt/UDISK/v2ray-asus-router
./init.sh
```

## Login to router

Create a ssh key pair

```bash
ssh-keygen -t rsa -C "router-admin"
```

```bash
cat ~/.ssh/id_rsa.router-admin.pub
```

fill the content to [router admin page > Advanced Settings > System > Service > Authorized Keys]

```bash
Host router
    Hostname 192.168.50.1
    Port 22
    User {username}
    ServerAliveInterval 60
```

login to router

```bash
ssh router
```

## Create USB external dirver for ASUS Routers

### Format a USB driver to EXT4 File System

* (Commercial software: https://www.paragon-software.com/home/extfs-mac/)

* (Open-Source software on OSX)

Install HomeBrew

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Install e2fsprogs

```bash
brew install e2fsprogs
```

Lookup usb-disk

```bash
diskutil list
```

for example your usb-disk is `/dev/disk2s1`

```bash
diskutil unmountdisk /dev/disk2s1
sudo $(brew --prefix e2fsprogs)/sbin/mkfs.ext4 /dev/disk2s1
```

### Create SWAP

```bash
dd if=/dev/zero of=/tmp/mnt/UDISK/swap bs=1k count=1048576
mkswap /tmp/mnt/UDISK/swap
```

Test

```bash
swapon /tmp/mnt/UDISK/swap
```
