#!/bin/sh
sed -i "s~__BASEPATH__~$(pwd)~" start-up.sh
sed -i "s~__BASEPATH__~$(pwd)~" post-mount
cp ./post-mount /jffs/scripts/post-mount
chmod +x /jffs/scripts/post-mount

./start-up.sh
cru a ScheduledReboot "*/1 * * * * $(pwd)/start-up.sh"