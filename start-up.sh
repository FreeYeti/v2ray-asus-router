#!/bin/sh
base_path=__BASEPATH__
v2ray_core=$base_path/v2ray

mkdir -p $base_path/log

pid=$(ps | grep -i  'v2ray' | grep 'config' | grep -v 'grep' |  awk '{print $1}')

if [ "$pid" ]
then
    echo "v2ray pid: $pid"
else
    ulimit -n 65536
    nohup $v2ray_core/v2ray -config $base_path/config.json > $base_path/log/v2ray.log &
    echo "time: $(date +%Y-%m-%d) $(date +%H:%M:%S)" >> $base_path/log/start-up.log

    sh $base_path/iptables.sh
    echo "v2ray started"
fi